import java.util.Iterator;

/**
 * @author Edward Rodriguez
 * SparseVector stores a linked list sparse vector of type element and
 * methods to add, subtract, and multiply two vectors
 *
 */
public class SparseVector {

	private DoublyLinkedList<Element> vector;
	private Element element;
	
	/**
	 * Contructor initializing element and vector
	 */
	public SparseVector() {
		element = new Element();
		vector = new DoublyLinkedList<Element>();
	}

	/**
	 * Contructor initializing element and vector
	 * @param sv vector to copy 
	 */
	public SparseVector(DoublyLinkedList<Element> sv) {
		element = new Element();
		vector = sv;
	}
	
	public DoublyLinkedList<Element> getVector() {
		return vector;
	}

	public void setVector(DoublyLinkedList<Element> vector) {
		this.vector = vector;
	}

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 * method to properly print a sparse vector
	 * 		Format: ([index, value], ..., )
	 */
	public String toString(){
		Iterator<Element> itr = vector.iterator();
		System.out.print("( ");
		while( itr.hasNext() ){
			itr.next().toString();
			if (itr.hasNext()) System.out.print(", ");
		}
		System.out.print(" )" + "\n");
		return null;	
	}
	
	/**
	 * create a vector from an array of type String 
	 * convert elements of the array to type int, and double
	 * to represent index, and value then add to object's vector
	 * @param s the String array to be converted
	 */
	public void createVector(String [] s){
		vector.clear();
		if (s.length % 2 == 0){
			for(int k = 0; k < s.length; k+= 2){
				Element temp = new Element();
				temp.setIndex(Integer.parseInt(s[k]));
				temp.setValue(Double.parseDouble(s[k + 1]));
				vector.add(temp);
			}
		}
		
		else
			throw new IllegalArgumentException("Please enter a proper sparse vector in file! aborting...");
	}
	
	/**
	 * sorts elements of the vector in increasing index order
	 * via bubblesort
	 */
	public void sort()
	{
		int size = vector.size();
		Element temp = new Element();
		
		for(int i = 0; i < size; i++)
		{
			for(int j = 1; j < (size - i); j++)
			{
				if(vector.get(j - 1).compareTo(vector.get(j)) > 0)
				{
					temp.setIndex(vector.get(j - 1).getIndex()); 
					temp.setValue(vector.get(j - 1).getValue()); 
					vector.get(j - 1).setIndex(vector.get(j).getIndex());
					vector.get(j - 1).setValue(vector.get(j).getValue());
					vector.get(j).setIndex(temp.getIndex());
					vector.get(j).setValue(temp.getValue());
				}
			}
		}
	}

	/**
	 * add a sparse vector and return new sparse vector sorted
	 * @param sv the sparse vector to be added
	 * @return the sum sparse vector sorted
	 */
	public  SparseVector add(SparseVector sv){
		DoublyLinkedList<Element> sum = new DoublyLinkedList<>();
		SparseVector temp = new SparseVector();
		
		if (vector.size() >= sv.getVector().size()){
			sum = add(this.getVector(), sv.getVector());
		}
		else {
			sum = add(sv.getVector(), this.getVector());
		}
		temp.setVector(sum);
		temp.sort();
		return temp; 
	}
	
	/**
	 * adds two vectors together and returns the sum vector
	 * compares each of the vectors element indexes to one another
	 * and adds the value of those elements w/ matching indexes together
	 * @param vec vector to be added
	 * @param vec2 the vector to be added to vec
	 * @return the sum vector
	 */
	private DoublyLinkedList<Element> add(DoublyLinkedList<Element> vec, DoublyLinkedList<Element> vec2){
		DoublyLinkedList<Element> sum = new DoublyLinkedList<>();
		Boolean idxFound = false;
		for (int i = 0; i < vec.size() ; i++) {
			Element temp = new Element();
			int idxA = vec.get(i).getIndex();
			double valA = vec.get(i).getValue();
			temp.setIndex(idxA);
	
			if (i >= vec2.size()) 
				{temp.setValue(valA);}
			else{
				double valB = vec2.get(i).getValue();
				int idxB = vec2.get(i).getIndex();
				if (idxA == idxB){
					temp.setValue(valA + valB);
				}
				else {
					for (int j = 0; idxFound == false && j < vec2.size() ; j++){
						idxB = vec2.get(j).getIndex();
						valB = vec2.get(j).getValue();
						if (idxA == idxB){
							temp.setValue(valA + valB);
							idxFound = true;	
						}
					}
				if (idxFound == false) temp.setValue(valA);
				}
			}
			if (temp.getValue() != 0) sum.add(temp);
			idxFound = false;
			
			if (i < vec2.size()){
				int tempBidx = vec2.get(i).getIndex();
				double tempBval = vec2.get(i).getValue();
				boolean sameIndex = true;
				for (int j = 0; j < vec.size(); j++) {
					int tempAidx = vec.get(j).getIndex();
					if (tempBidx == tempAidx){
						sameIndex = true;
						break;
					}
					else sameIndex = false;
				}
				if (sameIndex == false){
					Element v = new Element();
					v.setIndex(tempBidx);
					v.setValue(tempBval);
					sum.add(v);
				}
			}
		}
		return sum;
		
	}
	
	/** subtract a vector from another and return the difference vector, sorted
	 * @param sv the vector to be subtracted
	 * @return the difference vector
	 */
	public  SparseVector subtract(SparseVector sv){
		DoublyLinkedList<Element> diff = new DoublyLinkedList<>();
		SparseVector temp = new SparseVector();
		
		if (vector.size() >= sv.getVector().size()){
			diff = subtract(this.getVector(), sv.getVector(), true );
		}
		else {
			diff = subtract(sv.getVector(), this.getVector(), false);
		}
		temp.setVector(diff);
		temp.sort();
		return temp; 
	}
	
	/**
	 * subtracts one vector from another and returns the difference vector
	 * compares each of the vectors element indexes to one another
	 * and subtracts the value of those elements w/ matching indexes together
	 * @param vec vector to be added
	 * @param vec2 the vector to be added to vec
	 * @return the sum vector
	 */
	private DoublyLinkedList<Element> subtract(DoublyLinkedList<Element> vec, DoublyLinkedList<Element> vec2, Boolean subtract2ndVec){
		DoublyLinkedList<Element> diff = new DoublyLinkedList<>();
		Boolean idxFound = false;
		for (int i = 0; i < vec.size() ; i++) {
			Element temp = new Element();
			int idxA = vec.get(i).getIndex();
			double valA = vec.get(i).getValue();
			temp.setIndex(idxA);
	
			if (i >= vec2.size()){
				if (subtract2ndVec == true) temp.setValue(valA);
				else temp.setValue(-valA);
			}
			
			else{
				double valB = vec2.get(i).getValue();
				int idxB = vec2.get(i).getIndex();
				if (idxA == idxB){
					if (subtract2ndVec == true) temp.setValue(valA - valB);
					else temp.setValue(valB - valA);
					}
				else {
					for (int j = 0; idxFound == false && j < vec2.size() ; j++){
						idxB = vec2.get(j).getIndex();
						valB = vec2.get(j).getValue();
						if (idxA == idxB){
							if (subtract2ndVec == true) temp.setValue(valA - valB);
							else temp.setValue(valB - valA);
							idxFound = true;	
						}
					}
					if (idxFound == false){
						if (subtract2ndVec == true) temp.setValue(valA);
						else temp.setValue(-valA);
					}
				}
			}
			if (temp.getValue() != 0) diff.add(temp);
			idxFound = false;
			
			if (i < vec2.size()){
				int tempBidx = vec2.get(i).getIndex();
				double tempBval = vec2.get(i).getValue();
				boolean sameIndex = true;
				for (int j = 0; j < vec.size(); j++) {
					int tempAidx = vec.get(j).getIndex();
					if (tempBidx == tempAidx){
						sameIndex = true;
						break;
					}
					else sameIndex = false;
				}
				if (sameIndex == false){
					Element v = new Element();
					v.setIndex(tempBidx);
					if (subtract2ndVec == true) v.setValue(-tempBval);
					else v.setValue(tempBval);
					diff.add(v);
				}
			}
		}
		return diff;
		
	}
	
	/**
	 * produces the dot product of two vectors and returns the product, a scalar value.
	 * If an element with a particular  index  exists  in  both  vectors  then 
	 * multiply the values of the elements together and add
	 * the result to the dot product answer
	 * @param vec the vector to multiply
	 * @return the sum of the products
	 */
	public  double dot(SparseVector vec){
		DoublyLinkedList<Element> temp = this.getVector();
		DoublyLinkedList<Double> values= new DoublyLinkedList<>();
		Boolean sameIndexFound = false;
		double product, sum = 0, valA, valB = 0.0;
		int idxA, idxB;
		for (int i = 0; i < temp.size() ; i++) {
			idxA = temp.get(i).getIndex();
			valA = temp.get(i).getValue();
			sameIndexFound = false;
			for (int j = 0; j < vec.getVector().size()	&& sameIndexFound == false ; j++) {
				idxB = vec.getVector().get(j).getIndex();
				valB = vec.getVector().get(j).getValue();
				if (idxA == idxB){
					product = valA*valB;
					sum+= product;
					values.add(valA);
					values.add(valB);
					sameIndexFound = true;
				}
			}
		}
		return Math.floor(sum * 100) / 100; 
	}		
}
