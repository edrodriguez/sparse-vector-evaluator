import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Edward Rodriguez
 * Date: 10/27/16
 * 
 * Program that reads one or more vector/vector operations from a .txt file
 * and performs the given operation on said vectors
 *
 */
public class Project1 {
	
	public static char dotCharacter = (char) 183;
	public static String file = "project2.txt";
	public static String space = "\\s+";
	public static String add = "(?i:.*ADD.*)";
	public static String subtract = "(?i:.*SUBTRACT.*)";
	public static String dot = "(?i:.*DOT.*)";
	public static String plus = "+";
	public static String minus = "-";
	public static String equals = "=";
	public static String errorMessage = "Wrong Operation! " +
	"Enter add, subtract, or dot as an operation! aborting...";
	
	/**
	 * Main method opens the file "project1.txt" then reads 3 lines at a time
	 * until end of file is reached. First two lines are stored in an array of 
	 * type String and passed to the SparseVector class to convert to vectors.
	 * 3rd line represents the operation to be performed on the vectors. SparseVector 
	 * class then performs the given operation.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		
		try {
			FileInputStream fstream = new FileInputStream(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));
			
			SparseVector sv1 = new SparseVector();
			SparseVector sv2 = new SparseVector();
			String [] line1, line2;
			String operation, line = null;

			while ((line = reader.readLine()) != null){
				if (!line.isEmpty()) {
					line1 = line.split(space);
					sv1.createVector(line1);
					
					line2 = reader.readLine().split(space);
					sv2.createVector(line2);
					operation = reader.readLine().replaceAll(space, "");
					
					if (operation.matches(add)){
						sv1.toString();
						System.out.println(plus);
						sv2.toString();
						System.out.println(equals);
						sv1.add(sv2).toString();	
					}
					else if (operation.matches(subtract)){
						sv1.toString();
						System.out.println(minus);
						sv2.toString();
						System.out.println(equals);
						sv1.subtract(sv2).toString();	
					}
					else if (operation.matches(dot)){
						sv1.toString();
						System.out.println(dotCharacter);
						sv2.toString();
						System.out.println(equals);
						System.out.println(sv1.dot(sv2));
					}
					else throw new IllegalArgumentException(errorMessage);
					System.out.println("\n");
			    }	
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();	
		}	
	}
}