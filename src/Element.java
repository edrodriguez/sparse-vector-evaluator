
/**
 * @author Edward Rodriguez
 *Element stores the index and value of an element of a vector
 */
public class Element implements Comparable<Element>{
	
	private int index;
	private double value;
	
	public Element() {
		index = 0;
		value = 0;
	}
	
	public Element(int idx, double val) {
		index = idx;
		value = val;
	}
	
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		System.out.print("[" + index +", " + value + "]");
		return null;	
	}

	@Override
	public int compareTo(Element o) {
		if (this.index < o.getIndex())
			return -1;
		else if (this.index > o.getIndex())
			return 1;
		else
		// TODO Auto-generated method stub
		return 0;
	}

}
